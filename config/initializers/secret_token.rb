# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FirstApp::Application.config.secret_key_base = 'b3325db1dd9e08d1156647fdc39697d36dfa50d10fcb91e91283194cd9a8dd01950c6e7d48639a36007354c3fec699218d7201b094c5763064dceae6034655bc'
